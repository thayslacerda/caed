module.exports = app => {
  const Itens = app.data.Itens;
  const controller = {};


  //SALVA A CORREÇÃO DE UMA DETERMINADA QUESTÃO
  controller.salvar = (req, res) => {
    const id = req.params.idCorrecao;
    const achou = false;

    if(Itens[i]["situacao"] == "CORRIGIDO"){
      res.status(200).json({
        "situacao": "ERRO",
        "tipo": "ITEM_CORRIGIDO",
        "descrição": "Item já corrigido"
      });
    }

    for (var i = 0; i < Itens.length; i++) {
      if(id == Itens[i]["id"]){
        achou = true;
        Itens[i]["situacao"] = "CORRIGIDO";
      }
    }

    if(!achou){
      res.status(200).json("Não existe a correção "+ id);
    }else{
      res.status(200).json({
        "situacao": "SUCESSO",
        "descrição": "Correção salva com sucesso"
      })
    }

  }

  //ADICIONA UMA DETERMINADA CORREÇÃO NA LISTA DE CORREÇÕES RESERVADAS
  controller.reservadasId = (req, res) => {
    const id = req.params.idCorrecao;

    for (var i = 0; i < Itens.length; i++) {
      if(Itens[i]["id"] == id){

        if(Itens[i]["situacao"] == "CORRIGIDA"){
          res.status(200).json({
            "situacao": "ERRO",
            "tipo": "ITEM_CORRIGIDO",
            "descrição": "Item já corrigido"
          });
        }

        if(Itens[i]["situacao"] == "RESERVADA"){
          res.status(200).json({
            "situacao": "ERRO",
            "tipo": "ITEM_RESERVADO",
            "descrição": "Item já reservado"
          });
        }

        if(Itens[i]["situacao"] == "DISPONIVEL"){
          Itens[i]["situacao"] = "RESERVADA";
          res.status(200).json({
            "situacao": "SUCESSO",
            "descrição": "Correção reservada com sucesso"
          })
        }

        var chave = Itens[i]["chave"][0]["id"][0]["opcoes"];

        if(chave[0]["valor"] < 0 && chave[0]["valor"] > 1){
          res.status(200).json({
            "situacao": "ERRO",
            "tipo": "CHAVE_INCORRETA",
            "descrição": "Chave de correção incorreta. Valor '"+Itens[i]["chave"]["valor"]+"' não é válido para o item '"+Itens[i]["chave"]+"'"
          })
        }
      }
    }

    res.status(200).json({
      "situacao": "ERRO",
      "tipo": "CHAVE NÃO ENCONTRADA"
    })

  }

  //RETORNA A PRÓXIMA QUESTÃO "DISPONÍVEL" ou "RESERVADA"
  controller.proxima = (req, res) => {
    const reservada = req.params.reservada;

    for (var i = 0; i < Itens.length; i++) {
      reservada == true ? 
        (Itens[i]["situacao"] == "DISPONIVEL" || Itens[i]["situacao"] == "RESERVADA") &&
        res.status(200).json(Itens[i])
      : 
        (Itens[i]["situacao"] == "DISPONIVEL") &&
          res.status(200).json(Itens[i])
    }

    res.status(200).json({
      "data": null,
      "situacao": "ERRO",
      "tipo": "SEM_CORRECAO",
      "descrição": "Não existem mais correções disponíveis"
    })
  }


  //RETORNA TODAS AS QUESTÕES RESERVADAS
  controller.reservadas = (req, res) => {
    reservadas = new Array();

    for (var i = 0; i < Itens.length; i++) {
      (Itens[i]["situacao"] == "RESERVADA") &&
      reservadas.push(Itens[i]);
    }

    res.status(200).json(reservadas)
  }

  return controller;
}