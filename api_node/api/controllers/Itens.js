module.exports = app => {
  const Itens = app.data.Itens;
  const controller = {};

  controller.listItens = (req, res) => res.status(200).json(Itens);

  return controller;
}