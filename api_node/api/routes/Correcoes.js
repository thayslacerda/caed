module.exports = app => {
  const controller = app.controllers.Correcoes;

  app.route('/correcoes/proxima').get(controller.proxima);
  app.route('/correcoes/reservadas').get(controller.reservadas);

  app.route('/correcoes/:idCorrecao').post(controller.salvar);
  app.route('/correcoes/reservadas/:idCorrecao').post(controller.reservadasId);

}